<?php declare(strict_types=1);


namespace SwagShopFinder\Core\Api;


use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\Country\Exception\CountryNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"api"})
 */
class DemoDataController extends AbstractController
{
    /**
     * @var EntityRepositoryInterface
     */
    private $countryRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $shopFinderRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;



    public function __constuct(EntityRepositoryInterface $countryRepository, EntityRepositoryInterface $shopFinderRepository, LoggerInterface $logger){
        $this->countryRepository = $countryRepository;
        $this->shopFinderRepository = $shopFinderRepository;
        $this->logger = $logger;
    }

    /**
     *
     * @throws CountryNotFoundException
     * @Route("/api/v{version}/_actions/swag-shop-finder/generate", name="api.custom.swag_shop_finder.generate", methods={"POST"})
     */
    public function generate(Context $context): Response{

        var_dump($this->countryRepository); exit;
//        $this->logger->error('generate function demodata called');
//        exit;
//        $faker = Factory::create();
//        $country = $this->getActiveCountry($context);
//
//        $data = [];
//        for ($i = 0; $i < 50; $i++){
//            $data[] = [
//                'id' => Uuid::randomHex(),
//                'active' => true,
//                'name' => $faker->name,
//                'street' => $faker->streetAddress,
//                'postCode' => $faker->postcode,
//                'city' => $faker->city,
//                'countryId' => $country->getId()
//            ];
//        }
//        $this->shopFinderRepository->create($data, $context);
//        return new Response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Context $context
     * @return CountryEntity
     * @throws CountryNotFoundException
     * @throws InconsistentCriteriaIdsException
     */
    private function getActiveCountry(Context $context){
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('active', '1'));
        $criteria->setLimit(1);
        $country = $this->countryRepository->search($criteria, $context)->getEntities()->first();

        if ($country === null ){
            throw new CountryNotFoundException('CountryNotFound');
        }

        return $country;
    }
}
