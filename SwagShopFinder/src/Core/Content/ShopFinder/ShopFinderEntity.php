<?php declare(strict_types=1);

namespace SwagShopFinder\Core\Content\ShopFinder;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\Country\CountryEntity;

class ShopFinderEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var boolean|false
     */
    protected $active;
    /**
     * @var string|null
     */
    protected $name;
    /**
     * @var string|null
     */
    protected $street;
    /**
     * @var string|null
     */
    protected $post_code;
    /**
     * @var string|null
     */
    protected $city;
    /**
     * @var string|null
     */
    protected $url;
    /**
     * @var string|null
     */
    protected $telephone;
    /**
     * @var string|null
     */
    protected $open_times;
    /**
     * @var CountryEntity|null
     */
    protected $country;

    /**
     * @return bool|false
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param bool|false $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     */
    public function setStreet(?string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string|null
     */
    public function getPostCode(): ?string
    {
        return $this->post_code;
    }

    /**
     * @param string|null $post_code
     */
    public function setPostCode(?string $post_code): void
    {
        $this->post_code = $post_code;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     */
    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string|null
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    /**
     * @param string|null $telephone
     */
    public function setTelephone(?string $telephone): void
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string|null
     */
    public function getOpenTimes(): ?string
    {
        return $this->open_times;
    }

    /**
     * @param string|null $open_times
     */
    public function setOpenTimes(?string $open_times): void
    {
        $this->open_times = $open_times;
    }

    /**
     * @return CountryEntity|null
     */
    public function getCountry(): ?CountryEntity
    {
        return $this->country;
    }

    /**
     * @param CountryEntity|null $country
     */
    public function setCountry(?CountryEntity $country): void
    {
        $this->country = $country;
    }

}
